﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    [SerializeField] int numberOfPlayers;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void NumberOfPlayers(int i)
    {
        //take in the number of players that will be playing, and start the game
        numberOfPlayers = i;
        SceneManager.LoadScene("GameScene");
    }

    public int NumberOfPlayers()
    {
        //return the number of players that will be playing
        return numberOfPlayers;
    }
}
