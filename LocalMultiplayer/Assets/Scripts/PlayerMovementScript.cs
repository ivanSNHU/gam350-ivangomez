﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementScript : MonoBehaviour
{
    private BoxCollider2D bc;
    private Rigidbody2D rb;
    [SerializeField] float addForceHorizontal = 1000;
    [SerializeField] float addForceVertical = 1000;
    public string playerName;
    public bool canJump = false;


    // Start is called before the first frame update
    void Start()
    {
        bc = gameObject.GetComponent<BoxCollider2D>();
        rb = gameObject.GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //if you're alive you can move
        //if you're alive and your feet are on the ground, you can jump
        float dt = Time.deltaTime;
        byte currentHealth = gameObject.GetComponent<PlayerAttackScript>().CurrentHealth();
        bool alive;
        if (currentHealth > 0)
        {
            alive = true;
        }
        else
        {
            alive = false;
        }
        if (alive)
        {
            try
            {
                rb.AddForce(Vector2.right * Input.GetAxis("L_XAxis_" + playerName) * dt * addForceHorizontal);
                if (Input.GetButton("A_" + playerName) && canJump)
                {

                    rb.AddForce(Vector2.up * dt * addForceVertical);
                    canJump = false;

                }
            }
            catch(Exception)
            {
                alive = false;
            }            
        }
    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(rb.velocity.x * .9f, rb.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
    }
}
