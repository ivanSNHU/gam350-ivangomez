﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttackScript : MonoBehaviour
{
    PlayerMovementScript pms;
    Rigidbody2D rb2d;
    [SerializeField] float attackSpeed;
    [SerializeField] GameObject rotationPoint;
    [SerializeField] GameObject blade;
    [SerializeField] float rotationAngle;
    [SerializeField] Vector2 movementVector;
    [SerializeField] Vector2 absVector;
    [SerializeField] float calculatedAngle;
    [SerializeField] byte health;
    float lastAttack;

    // Start is called before the first frame update
    void Start()
    {
        pms = GetComponent<PlayerMovementScript>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        lastAttack += dt;

        //update some variables
        movementVector = rb2d.velocity;
        movementVector.Normalize();
        absVector = new Vector2(Mathf.Abs(movementVector.x), Mathf.Abs(movementVector.y));
        Vector2 min = new Vector2(1, 0);
        calculatedAngle = Vector2.SignedAngle(min, absVector);
        rotationAngle = Vector2.SignedAngle(min, absVector);

        //check health
        bool alive;
        if (health > 0)
        {
            alive = true;
        }
        else
        {
            alive = false;
        }

        //attack in the direction of movement if you can attack again now, and are alive
        if (Input.GetButtonDown("X_" + pms.playerName) && lastAttack > attackSpeed && alive)
        {
            Attack();
        }
    }

    void Attack()
    {
        //attack in the direction of movement funtion
        float finalAttackAngles = rotationAngle;
        Vector2 finalAttackVector = movementVector;
        //summon some attack things
        if (finalAttackVector.magnitude > 0.1f)
        {
            if (finalAttackVector.x > 0 && finalAttackVector.y > 0)
            {
                //do nothing
            }
            else if (finalAttackVector.x < 0 && finalAttackVector.y > 0)
            {
                finalAttackAngles += 90;
            }
            else if (finalAttackVector.x < 0 && finalAttackVector.y < 0)
            {
                finalAttackAngles -= 180;
            }
            else if (finalAttackVector.x > 0 && finalAttackVector.y < 0)
            {
                finalAttackAngles -= 90;
            }
            else if (finalAttackVector.x == 1 && finalAttackVector.y == 0)
            {
                finalAttackAngles = 0;
            }
            else if (finalAttackVector.x == 0 && finalAttackVector.y == 1)
            {
                finalAttackAngles = 90;
            }
            else if (finalAttackVector.x == -1 && finalAttackVector.y == 0)
            {
                finalAttackAngles = 180;
            }
            else if (finalAttackVector.x == 0 && finalAttackVector.y == -1)
            {
                finalAttackAngles = -90;
            }
            rotationPoint.transform.eulerAngles = new Vector3(0, 0, finalAttackAngles);
            blade.SetActive(true);
        }
        lastAttack = 0;
        //gameObject.
    }

    public byte CurrentHealth()
    {
        return health;
    }

    public void TakeDamage()
    {
        //take damage. unless dead, then you're back in the game!
        if (health > 0)
        {
            health--;
        }
        else if (health == 0)
        {
            health = 3;
        }
    }
}
