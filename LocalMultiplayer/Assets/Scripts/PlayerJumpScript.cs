﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJumpScript : MonoBehaviour
{
    PlayerMovementScript playerMovementScript;

    // Start is called before the first frame update
    void Start()
    {
        playerMovementScript = GetComponentInParent<PlayerMovementScript>();
    }

    // Update is called once per frame
    void Update()
    {
        //this script check to see if you're feet are on the ground
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        playerMovementScript.canJump = true;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        playerMovementScript.canJump = false;
    }
}
