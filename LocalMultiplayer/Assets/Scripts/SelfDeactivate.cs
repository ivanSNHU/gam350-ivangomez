﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelfDeactivate : MonoBehaviour
{
    [SerializeField] float timeToLive;
    float timeLived;

    // Start is called before the first frame update
    void Start()
    {
        //this script governs the attack or lance objects. they deal damage if they touch a player, and disappear after their timeToLive
        timeLived = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timeLived += Time.deltaTime;
        if (timeLived > timeToLive)
        {
            Deactivate();
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
        {
            //deal damage
            collision.collider.gameObject.GetComponent<PlayerAttackScript>().TakeDamage();
            Deactivate();
        }
    }

    void Deactivate()
    {
        timeLived = 0;
        gameObject.SetActive(false);
    }
}
