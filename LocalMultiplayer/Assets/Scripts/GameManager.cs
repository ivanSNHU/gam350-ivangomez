﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    List<GameObject> players = new List<GameObject>();
    [SerializeField] int numberOfPlayers = 0;
    [SerializeField] GameObject playerPrefab;
    [SerializeField] GameObject VictoryText;
    [SerializeField] Sprite[] sprite = new Sprite[4];

    class ColorOption
    { //this class is currently unused
        byte red, green, blue;
        ColorOption()
        {
            red = 0;
            green = 0;
            blue = 0;
        }
        ColorOption(byte red, byte green, byte blue)
        {
            this.red = red;
            this.green = green;
            this.blue = blue;
        }
    }

    class Player
    { //this class is currently unused
        string playerName;
        ColorOption colorOption;
    }

    // Start is called before the first frame update
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
        //get the number of players from the menu object
        numberOfPlayers = GameObject.Find("HowManyPlayers").GetComponent<MenuScript>().NumberOfPlayers();
    }

    private void Start()
    {
        //create our players
        for (int i = 0; i < numberOfPlayers;)
        {
            GameObject newPlayer = Instantiate(playerPrefab);

            newPlayer.transform.position = newPlayer.transform.position + new Vector3(i * 3, 0, 0);
            newPlayer.GetComponent<SpriteRenderer>().sprite = sprite[i];
            ++i;
            newPlayer.GetComponent<PlayerMovementScript>().playerName = i.ToString();

            players.Add(newPlayer);
        }
    }

    // Update is called once per frame
    void Update()
    {
        //count the number of live players, and potentially show victory text
        numberOfPlayers = players.Count;
        int numberOfLivePlayers = 0;
        foreach (var p in players)
        {
            if (p.GetComponent<PlayerAttackScript>().CurrentHealth() > 0)
            {
                numberOfLivePlayers++;
            }
        }

        if (numberOfLivePlayers <= 1)
        {
            VictoryText.SetActive(true);
        }
        else
        {
            VictoryText.SetActive(false);
        }
    }
}