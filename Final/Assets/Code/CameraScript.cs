﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    public static CameraScript instance;
    public GameObject pl;
    [SerializeField] float strength = 1;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != this && instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        Vector3 targetPosition = new Vector3(
            Mathf.Lerp(transform.position.x, pl.transform.position.x, dt * strength),
            transform.position.y,
            Mathf.Lerp(transform.position.z, pl.transform.position.z, dt * strength));
        transform.position = targetPosition;
    }
}
