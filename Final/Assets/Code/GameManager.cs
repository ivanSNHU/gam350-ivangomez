﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    float lastTag = 0f;
    float bufferTime = 1.0f;

    // Start is called before the first frame update
    void Start()
    {
        if(instance != this && instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    // Update is called once per frame
    void Update()
    {
        lastTag += Time.deltaTime;
    }

    public void Tag(GameObject tagger, GameObject taggee)
    {
        if(lastTag > bufferTime && tagger.GetComponent<ItScript>().it)
        {
            tagger.GetComponent<ItScript>().it = false;
            taggee.GetComponent<ItScript>().it = true;
            lastTag = 0f;
        }
    }
}
