﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItScript : MonoBehaviour
{
    public bool it = false;
    MeshRenderer mr;
    [SerializeField] Material playerMaterial;
    [SerializeField] Material nonplayerMaterial;
    [SerializeField] Material itMaterial;

    // Start is called before the first frame update
    void Start()
    {
        mr = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (it)
        {
            mr.material = itMaterial;
        }
        else if (GetComponent<PlayerScript>().enabled)
        {
            mr.material = playerMaterial;
        }
        else
        {
            mr.material = nonplayerMaterial;
        }
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if(it)
    //    {
    //        ItScript itScript;
    //        if(collision.gameObject.TryGetComponent<ItScript>(out itScript))
    //        {
    //            GameManager.instance.Tag(gameObject, collision.gameObject);
    //        }            
    //    }        
    //}

    public void SetIt()
    {
        it = true;
    }

    public void SetNotIt()
    {
        it = false;
    }
}
