﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChange : MonoBehaviour
{
    ClientNetwork clientNet;

    // Start is called before the first frame update
    void Start()
    {
        clientNet = GameObject.Find("ExampleClient").GetComponent<ClientNetwork>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            clientNet.CallRPC("ChangeColor", UCNetwork.MessageReceiver.AllClients, -1,
                gameObject.GetComponent<NetworkSync>().NetworkId);
        }
    }
}
