﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class TacticsClient : MonoBehaviour
{  
    public enum GameState
    {
        Login,
        Starting,
        Playing
    }

    public class Map
    {
        public int sizeX, sizeY;
        public class Space
        {
            public bool isBlocked = false;
            public Space()
            {
                isBlocked = false;
            }
        }
        public Space[,] spaces;
        public void initializeSpaces(Image blocked, Image empty)
        {
            spaces = new Space[sizeX, sizeY];
            drawSpaces(blocked, empty);
        }
        public Map()
        {
            sizeX = 1;
            sizeY = 1;
            //initializeSpaces();
        }
        public void drawSpaces(Image blocked, Image empty)
        {
            for (int x = 0; x < sizeX; x++)
            {
                for (int y = 0; y < sizeY; y++)
                {
                    if (spaces[x, y].isBlocked)
                    {
                        Image newImage = Instantiate(blocked);
                        newImage.transform.position = new Vector3(
                            x,
                            y,
                            0);
                    }
                    else
                    {
                        Image newImage = Instantiate(empty);
                        newImage.transform.position = new Vector3(
                            x,
                            y,
                            0);
                    }
                }
            }
        }
    }

#region variables

public GameObject playerPrefab;
    public ClientNetwork clientNet;
    public Dictionary<int, Player> players = new Dictionary<int, Player>();
    public Map map = new Map();

    // Are we in the process of logging into a server
    private bool loginInProcess = false;
    public GameObject loginScreen;
    public GameObject gameScreen;
    public int myPlayerId = 0;
    public string myPlayerName = "";
    public int myPlayerClass = 1;
    public int activePlayerId = 0;
    public int gameStartTimer = 0;
    Canvas mapCanvas;
    public Image emptyTile;
    public Image blockedTile;

    #endregion

    public void initializeSpaces()
    {
        map.initializeSpaces(blockedTile, emptyTile);
    }

    // Use this for initialization
    void Awake()
    {
        loginScreen.SetActive(true);
        gameScreen.SetActive(false);
        //clientNet.CallRPC(string aFunctionName, MessageReceiver aReceiver, int aNetworkId, params object[] aParams)

        // Make sure we have a ClientNetwork to use
        if (clientNet == null)
        {
            clientNet = GetComponent<ClientNetwork>();
        }
        if (clientNet == null)
        {
            clientNet = (ClientNetwork)gameObject.AddComponent(typeof(ClientNetwork));
        }
    }

    // Start the process to login to a server
    public void ConnectToServer(string aServerAddress, int aPort)
    {
        if (loginInProcess)
        {
            return;
        }
        loginInProcess = true;

        ClientNetwork.port = aPort;
        clientNet.Connect(aServerAddress, ClientNetwork.port, "", "", "", 0);
    }

    void OnNetStatusConnected()
    {
        loginScreen.SetActive(false);
        gameScreen.SetActive(true);
        clientNet.AddToArea(1);
    }

    void OnDestroy()
    {
        if (clientNet.IsConnected())
        {
            clientNet.Disconnect("Peace out");
        }
    }

    public void attackHandler(int playerId, int x, int y)
    {
        //do nothing
    }

    public void chatHandler(string message)
    {
        //do nothing
    }
}