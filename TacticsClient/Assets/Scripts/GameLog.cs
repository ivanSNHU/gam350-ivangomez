﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLog : MonoBehaviour
{
    public static GameLog instance;
    [SerializeField] Text displayTextBox;
    List<string> log = new List<string>();
    RPCMan rpc;

    // Start is called before the first frame update
    void Start()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
        rpc = RPCMan.instance;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Log(string s)
    {
        log.Insert(0, s);
        displayLog();
    }

    void displayLog()
    {
        string temp = "";
        foreach (var message in log)
        {
            temp.Insert(0, message + "\n");
        }
        displayTextBox.text = temp;
    }
}
