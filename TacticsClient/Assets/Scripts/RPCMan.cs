﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RPCMan : MonoBehaviour
{
    [SerializeField] TacticsClient tc;
    public static RPCMan instance;
    [SerializeField] ClientNetwork clientNet;
    [SerializeField] GameObject playerPrefab;
    [SerializeField] Toggle pingPong;
    [SerializeField] Text x, y;

    //Debug.Log("");

    //Login Phase:
    //Messages from the client to the server:
    public void SetName(string name)// - Set the name of your character
    {
        Debug.Log("SetName");
        clientNet.CallRPC("SetName", UCNetwork.MessageReceiver.ServerOnly, -1, name);
        tc.players[tc.myPlayerId].name = name;
    }
    public void SetCharacterType(int type)// - Set the type of character you would like to be (see id’s above)
    {
        Debug.Log("SetCharacterType");
        clientNet.CallRPC("SetCharacterType", UCNetwork.MessageReceiver.ServerOnly, -1, type);
        tc.players[tc.myPlayerId].playerClass = intToClass(type);
    }
    public void Ready(bool isReady)// - Call when your client is ready to play, or is no longer ready to play
    {
        Debug.Log("Ready");
        clientNet.CallRPC("isReady", UCNetwork.MessageReceiver.ServerOnly, -1, isReady);
    }

    //Messages from the server to the client:
    public void SetPlayerId(int playerId)// - Called to tell your client what your player id will be for the session
    {
        Debug.Log("SetPlayerId");
        tc.myPlayerId = playerId;
        //Player newb = new Player();
        GameObject newMe = Instantiate(playerPrefab);
        Player newb = newMe.GetComponent<Player>();
        newb.playerId = playerId;
        //newb.preFab = tc.playerPrefab;
        //clientNet.Instantiate("Player", Vector3.zero, Quaternion.identity);
        tc.players.Add(tc.myPlayerId, newb);
        initializeMe();
    }
    public void SetTeam(int team)// - Team will be 1 or 2
    {
        Debug.Log("SetTeam");
        tc.players[tc.myPlayerId].teamId = team;
    }
    public void NewPlayerConnected(int playerId, int team)// - Another player has connected to the game
    {
        Debug.Log("NewPlayerConnected");
        GameObject newMe = Instantiate(playerPrefab);
        Player newb = newMe.GetComponent<Player>();
        //TacticsClient.Player newb = new TacticsClient.Player();
        newb.playerId = playerId;
        newb.teamId = team;
        newb.isConnected = true;
        tc.players.Add(playerId, newb);
    }
    public void PlayerNameChanged(int playerId, string name)// - Another player has changed their name
    {
        Debug.Log("PlayerNameChanged");
        tc.players[playerId].name = name;
    }
    public void PlayerIsReady(int playerId, bool isReady)// - Another player is ready to play
    {
        Debug.Log("PlayerIsReady");
        tc.players[playerId].isReady = isReady;
    }
    public void PlayerClassChanged(int playerId, int type)// - Another player has changed their character type
    {
        Debug.Log("PlayerClassChanged");
        tc.players[playerId].playerClass = intToClass(type);
    }
    public void GameStart(int time)// - The game will start after the given amount of time
    {
        Debug.Log("GameStart");
        foreach (Player p in tc.players.Values)
        {
            // Set the initial values for the player based on their class
            if (p.playerClass == Player.PlayerClass.Warrior)
            {
                p.maxHealth = 100;
                p.maxMoves = 2;
                p.attackRange = 1;
            }
            else if (p.playerClass == Player.PlayerClass.Rogue)
            {
                p.maxHealth = 70;
                p.maxMoves = 5;
                p.attackRange = 1;
            }
            else if (p.playerClass == Player.PlayerClass.Wizard)
            {
                p.maxHealth = 30;
                p.maxMoves = 4;
                p.attackRange = 6;
            }
        }
        tc.gameStartTimer = time;
    }

    //Game Phase:
    //Messages from the client to the server:
    public void RequestMove(int x, int y)// - Ask the server to move to the specific position
    {
        Debug.Log("RequestMove");
        clientNet.CallRPC("RequestMove", UCNetwork.MessageReceiver.ServerOnly, tc.myPlayerId, x, y);
    }
    public void Move()
    {
        RequestMove(int.Parse(x.text), int.Parse(y.text));
    }
    public void RequestAttack(int x, int y)// - Attack the player at the given location, must be adjacent to your current position if Warrior or Rogue, must be within 6 units if Wizard
    {
        Debug.Log("RequestAttack");
        clientNet.CallRPC("RequestAttack", UCNetwork.MessageReceiver.ServerOnly, tc.myPlayerId, x, y);
    }
    public void Attack()
    {
        RequestAttack(int.Parse(x.text), int.Parse(y.text));
    }
    public void SendChat(string message)// - Send a chat message to everyone in the game
    {
        Debug.Log("SendChat");
        clientNet.CallRPC("SendChat", UCNetwork.MessageReceiver.ServerOnly, tc.myPlayerId, message);
    }
    public void SendTeamChat(string message)// - Send a chat message to your team
    {
        Debug.Log("SendTeamChat");
        clientNet.CallRPC("SendTeamChat", UCNetwork.MessageReceiver.ServerOnly, tc.myPlayerId, message);
    }
    public void PassTurn()// - Pass or end your turn
    {
        Debug.Log("PassTurn");
        clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, tc.myPlayerId);
    }

    //Messages from the server to the client:
    public void SetMapSize(int x, int y)// - Tell the client the size of the map
    {
        Debug.Log("SetMapSize");
        tc.map.sizeX = x;
        tc.map.sizeY = y;
        tc.initializeSpaces();
    }
    public void SetBlockedSpace(int x, int y)// - Tell the client that a specific space on the map is “blocked”
    {
        Debug.Log("SetBlockedSpace");
        tc.map.spaces[x, y].isBlocked = true;
    }
    public void SetPlayerPosition(int playerId, int x, int y)// - A player has moved and is now at the position given
    {
        Debug.Log("SetPlayerPosition");
        tc.players[playerId].xPosition = x;
        tc.players[playerId].yPosition = y;
        PingPong(playerId);
    }
    public void StartTurn(int playerId)// - Called when it is the start of the given player’s turn
    {
        Debug.Log("StartTurn");
        tc.activePlayerId = playerId;
    }
    public void AttackMade(int playerId, int x, int y)// - The given player just made an attack at the given location
    {
        Debug.Log("AttackMade");
        tc.attackHandler(playerId, x, y);
        PingPong(playerId);
    }
    public void DisplayChatMessage(string message)// - Display a chat message from another client or the server
    {
        Debug.Log("DisplayChatMessage");
        tc.chatHandler(message);
    }
    public void UpdateHealth(int playerId, int newHealth)// - Update the health of a player, will be called whenever a player’s health changes
    {
        Debug.Log("UpdateHealth");
        tc.players[playerId].health = newHealth;
    }


    // Start is called before the first frame update
    public void Start()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
        clientNet = FindObjectOfType<ClientNetwork>();
        tc = FindObjectOfType<TacticsClient>();
    }

    public Player.PlayerClass intToClass(int i)
    {
        switch (i)
        {
            case 1:
                return Player.PlayerClass.Warrior;
            case 2:
                return Player.PlayerClass.Rogue;
            case 3:
                return Player.PlayerClass.Wizard;
            default:
                return Player.PlayerClass.Warrior;
        }
    }

    public int classToInt(Player.PlayerClass i)
    {
        switch (i)
        {
            case Player.PlayerClass.Warrior:
                return 1;
            case Player.PlayerClass.Rogue:
                return 2;
            case Player.PlayerClass.Wizard:
                return 3;
            default:
                return 1;
        }
    }

    public void PingPong(int playerId)
    {
        if(playerId != tc.myPlayerId && pingPong.isOn)
        {
            Debug.Log("Ping pong");
            clientNet.CallRPC("PassTurn", UCNetwork.MessageReceiver.ServerOnly, playerId);
        }
    }

    public void initializeMe()
    {
        SetName(tc.myPlayerName);
        SetCharacterType(tc.myPlayerClass);
        Ready(true);
    }
}
