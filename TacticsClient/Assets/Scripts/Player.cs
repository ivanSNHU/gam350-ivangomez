﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public enum PlayerClass
    {
        Warrior = 1,
        Rogue = 2,
        Wizard = 3
    }

    //float speed = 5;

    public long clientId; // Unique client id
    public bool isConnected; // Is this client connected, false if they have been removed from the game

    public int playerId; // Assigned player id
    public int teamId; // Team id

    // Data that the client is allowed to set
    public string playerName;
    public bool isReady;
    public PlayerClass playerClass;

    // Position on the board
    public int maxMoves;
    public int movesRemaining;
    public int xPosition;
    public int yPosition;

    // Attack data
    public int attackRange;

    // Health
    public int maxHealth;
    public int health;    

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(
            xPosition,
            yPosition,
            0);
    }
}
