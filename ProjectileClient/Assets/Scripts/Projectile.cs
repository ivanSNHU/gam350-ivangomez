﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    [SerializeField] float velocity = 1000.0f;
    [SerializeField] float ttl = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        if (GetComponent<NetworkSync>().owned)
        {
            transform.position = transform.position + (transform.up * velocity * dt);
        }
        ttl -= dt;
        if (ttl <= 0)
        {
            Destroy(gameObject);
        }
    }
}
