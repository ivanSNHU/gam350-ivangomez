﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    [SerializeField] GameObject Dot;
    Pivot p;
    ClientNetwork clientNetwork;
    Camera cam;
    float speed = 5;

    private void Start()
    {
        p = GetComponentInChildren<Pivot>();
        clientNetwork = FindObjectOfType<ClientNetwork>();
        cam = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        if (GetComponent<NetworkSync>().owned)
        {
            Camera.main.GetComponent<CameraFollow>().followTarget = gameObject;
            Vector3 movement = new Vector3(Input.GetAxis("Horizontal") * speed * Time.deltaTime, Input.GetAxis("Vertical") * speed * Time.deltaTime, 0);
            transform.position += movement;

            GetComponentInChildren<Pivot>().Show();

            if (Input.GetButtonDown("Fire"))
            {
                Fire();
            }
        }
    }

    public void OnGainOwnership()
    {

    }

    private void Fire()
    {
        clientNetwork.Instantiate("Projectile", Dot.transform.position, Dot.transform.rotation);
    }
}
