﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pivot : MonoBehaviour
{
    [SerializeField] Vector2 inputMousePosition;
    //[SerializeField] Vector2 mousePosition;
    bool show = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (show)
        {
            //get mouse position        
            //convert mouse position to world position
            inputMousePosition = Input.mousePosition;
            //mousePosition = (Vector2)Camera.main.ScreenToWorldPoint(inputMousePosition);
            Camera.main.WorldToScreenPoint(transform.parent.position);
            //get player position
            Vector2 playerPosition = Camera.main.WorldToScreenPoint(transform.parent.position);
            //subtract player p from mouse p
            Vector2 angleVector = inputMousePosition - playerPosition;
            //get the angle
            float angleFloat = Vector2.SignedAngle(Vector2.up, angleVector);
            transform.localEulerAngles = new Vector3(0, 0, angleFloat);
            //instnatiate the bullet // apply rotation
            //clientNetwork.Instantiate("Projectile", transform.position, Quaternion.Euler(0, 0, angleFloat));
        }
    }

    public void Show()
    {
        show = true;
    }
}
