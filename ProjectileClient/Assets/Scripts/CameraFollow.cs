﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public GameObject followTarget;
    [SerializeField] float velocityOffsetScalar = 1.0f;
    [SerializeField] float followScalar = 1.0f;
    Vector2 velocityOffset;
    Vector2 previousPosition;
    Vector2 currentPosition;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float dt = Time.deltaTime;
        if(followTarget != null)
        {
            Vector2 computedTarget = ((Vector2)followTarget.transform.position) + velocityOffset;
            transform.position = new Vector3(
                Mathf.Lerp(transform.position.x, computedTarget.x, dt * followScalar),
                Mathf.Lerp(transform.position.y, computedTarget.y, dt * followScalar),
                transform.position.z);
        }
    }

    private void FixedUpdate()
    {
        previousPosition = currentPosition;
        currentPosition = transform.position;
        velocityOffset = (currentPosition - previousPosition) * Time.fixedDeltaTime * velocityOffsetScalar;
    }
}
