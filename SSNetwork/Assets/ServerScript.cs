﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml;
using System.IO;
using SSNetwork;

public class ServerScript : MonoBehaviour
{
    [SerializeField] int port = 13000;
    UdpClient serverClient;
    ReceiveArgs args;

    // Start is called before the first frame update
    void Start()
    {
        serverClient = new UdpClient(port, AddressFamily.InterNetwork);
        args = new ReceiveArgs(serverClient, new IPEndPoint(IPAddress.Any, port));
        BeginReceive();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel"))
        {
            Application.Quit();
        }
    }

    void BeginReceive()
    {
        serverClient.BeginReceive(SSNetwork.Functions.ReceiveCallback, args);
    }
}