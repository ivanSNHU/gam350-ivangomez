﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Xml;
using System.IO;
using SSNetwork;

public class ClientScript : MonoBehaviour
{
    [SerializeField] string ipString = "127.0.0.1";
    [SerializeField] int port = 13000;
    IPEndPoint serverEndPoint;
    UdpClient client;
    Message m = new Message("test", "102345");
    float speed = 5.0f;
    float delta = 0f;

    // Start is called before the first frame update
    void Start()
    {
        serverEndPoint = new IPEndPoint(IPAddress.Parse(ipString), port);
        client = new UdpClient(13001);
    }

    // Update is called once per frame
    void Update()
    {
        delta += Time.deltaTime;
        if (delta > speed)
        {
            Send(m);
            delta -= speed;
        }
    }

    void Send(Message message)
    {
        SendArgs sendArgs = new SendArgs(message, client, serverEndPoint);
        SSNetwork.Functions.SendMessage(sendArgs);
    }
}