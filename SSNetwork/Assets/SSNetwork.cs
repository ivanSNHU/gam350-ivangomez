﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace SSNetwork
{
    [Serializable]
    public class Message
    {
        public string type;
        public string value;

        public Message(string type, string value)
        {
            this.type = type;
            this.value = value;
        }
    }

    public class SendArgs
    {
        public Message message;
        public UdpClient udpClient;
        public IPEndPoint endPoint;
        public SendArgs(Message message, UdpClient udpClient, IPEndPoint endPoint)
        {
            this.message = message;
            this.udpClient = udpClient;
            this.endPoint = endPoint;
        }
    }

    public class ReceiveArgs
    {
        public UdpClient udpClient;
        public IPEndPoint endPoint;
        public ReceiveArgs(UdpClient udpClient, IPEndPoint endPoint)
        {
            this.udpClient = udpClient;
            this.endPoint = endPoint;
        }
    }

    public class Functions
    {
        public static void MessageHandler(Message message)
        {
            switch(message.type)
            {
                case "test":
                    Debug.Log(message.type + " " + message.value);
                    break;
                default:
                    Debug.Log("Unsupported message type: " + message.type + " with value " + message.value);
                    break;
            }            
        }

        public static void SendMessage(SendArgs args)
        {
            byte[] buffer = new byte[1024];
            MemoryStream stream = new MemoryStream(buffer);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(stream, args.message);
            stream.Close();
            args.udpClient.BeginSend(buffer, buffer.Length, args.endPoint, SendCallback, args);
        }

        public static void ReceiveCallback(IAsyncResult ar)
        {
            ReceiveArgs args = (ReceiveArgs)ar.AsyncState;
            byte[] buffer = args.udpClient.EndReceive(ar, ref args.endPoint);
            args.udpClient.BeginReceive(ReceiveCallback, args);

            MemoryStream stream = new MemoryStream(buffer);
            stream.Seek(0, SeekOrigin.Begin);
            Message message;
            BinaryFormatter bf = new BinaryFormatter();
            message = bf.Deserialize(stream) as Message;
            MessageHandler(message);
        }

        public static void SendCallback(IAsyncResult ar)
        {
            //do nothing
        }
    }
}
