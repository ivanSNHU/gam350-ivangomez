﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;

public class ExampleServer : MonoBehaviour
{
    public static ExampleServer instance;

    public ServerNetwork serverNet;

    public int portNumber = 603;

    int objectCurrentIt;
   
    // Use this for initialization
    void Awake()
    {
        instance = this;

        // Initialization of the server network
        ServerNetwork.port = portNumber;
        if (serverNet == null)
        {
            serverNet = GetComponent<ServerNetwork>();
        }
        if (serverNet == null)
        {
            serverNet = (ServerNetwork)gameObject.AddComponent(typeof(ServerNetwork));
            Debug.Log("ServerNetwork component added.");
        }

        //serverNet.EnableLogging("rpcLog.txt");
    }

    // A client has just requested to connect to the server
    void ConnectionRequest(ServerNetwork.ConnectionRequestInfo data)
    {
        Debug.Log("Connection request from " + data.username);

        serverNet.ConnectionApproved(data.id);

    }

    public void Update()
    {
        //ServerNetwork.NetworkObject obj = serverNet.GetNetObjById(objectCurrentIt);
        //obj.position

        /*
        Dictionary<int, ServerNetwork.NetworkObject> allObjs = serverNet.GetAllObjects();
        foreach (currObj in allObjs)
        {
            if (obj.position overlaps currObj.position)
            {
                tell obj its not it anymore
                tell currObj it is it
                objectCurrentIt = currObj.networkId;
            }
        }
        */

        //compare each object to all the others        
        var allObjects = serverNet.GetAllObjects();

        if(objectCurrentIt == 0)
        {
            Genesis(allObjects);
        }

        foreach (var x in allObjects)
        {
            foreach(var y in allObjects)
            {
                if(x.Key == y.Key)
                {
                    continue;
                }
                if(Vector3.Distance(x.Value.position, y.Value.position) <= 6.0f)
                {
                    Tag(x.Value, y.Value);
                }
            }
        }
    }

    private void Tag(ServerNetwork.NetworkObject x, ServerNetwork.NetworkObject y)
    {
        if(x.networkId == objectCurrentIt)
        {
            serverNet.CallRPC("SetIt", UCNetwork.MessageReceiver.AllClients, y.networkId);
            serverNet.CallRPC("SetNotIt", UCNetwork.MessageReceiver.AllClients, x.networkId);
        }
        else if(y.networkId == objectCurrentIt)
        {
            serverNet.CallRPC("SetIt", UCNetwork.MessageReceiver.AllClients, x.networkId);
            serverNet.CallRPC("SetNotIt", UCNetwork.MessageReceiver.AllClients, y.networkId);
        }
    }

    private void Genesis(Dictionary<int, ServerNetwork.NetworkObject> allObjs)
    {
        foreach(var e in allObjs)
        {
            serverNet.CallRPC("SetIt", UCNetwork.MessageReceiver.AllClients, e.Key);
            objectCurrentIt = e.Key;
        }
    }
}
